﻿using Duality;
using LiteNetLib;
using LiteNetLib.Utils;
using System.Collections.Generic;
using System;

namespace Headmower.Network
{
    public class NetUpdater : Component, ICmpInitializable, ICmpUpdatable
    {
        public ContentRef<NetClient> NetClient { get; set; }

        protected Dictionary<long, INetSyncer> SyncerList { get; set; } = new Dictionary<long, INetSyncer>();
        protected IDFactory IDFactory = new IDFactory();
        protected NetDataWriter Writer { get; set; } = new NetDataWriter();

        private NetSerializer Serializer { get; set; } = new NetSerializer();

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                NetClient.Res.NetworkReceiveEvent += OnNetworkReceiveEvent;
                NetClient.Res.Start();
                NetClient.Res.Connect();
            }
        }

        private void OnNetworkReceiveEvent(object sender, NetworkReceiveEventArgs e)
        {
            while (!e.Reader.EndOfData)
            {
                var objID = e.Reader.GetLong();
                SyncerList[objID].OnPacketReceived(e.Reader);
            }
            //Serializer.ReadAllPackets(e.Reader, e.Peer);
            //Serializer.ReadAllPackets(e.Reader, e.Peer);
        }

        public void OnShutdown(ShutdownContext context)
        {
            if (context == ShutdownContext.Deactivate)
            {
                NetClient.Res.NetworkReceiveEvent -= OnNetworkReceiveEvent;
                NetClient.Res.Stop();
            }
        }

        public void RegisterSyncer(INetSyncer syncer)
        {
            var id = IDFactory.RequestID();
            SyncerList.Add(id, syncer);
        }

        public void Send(INetSyncer syncer, NetDataWriter writer)
        {
            Writer.Put(_findSyncer(syncer));
            Writer.Put(writer.CopyData());
            NetClient.Res.Send(Writer);
            Writer.Reset();
        }

        public void UnregisterSyncer(INetSyncer syncer)
        {
            var id = _findSyncer(syncer);
            IDFactory.FreeID(id);
            SyncerList.Remove(id);
        }

        private long _findSyncer(INetSyncer syncer)
        {
            foreach (var val in SyncerList)
            {
                if (val.Value == syncer)
                {
                    return val.Key;
                }
            }
            throw new KeyNotFoundException();
        }

        public void OnUpdate()
        {
            NetClient.Res.PollEvents();
        }
    }
}