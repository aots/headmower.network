﻿using System.Collections.Generic;

namespace Headmower.Network
{
    public class IDFactory
    {
        private List<long> UsedIDs { get; set; } = new List<long>();

        public long RequestID()
        {
            for (long id = long.MinValue; id <= long.MaxValue; id++)
            {
                if (!UsedIDs.Contains(id))
                {
                    UsedIDs.Add(id);
                    return id;
                }
            }
            throw new KeyNotFoundException();
        }

        public void FreeID(long id)
        {
            UsedIDs.Remove(id);
        }
    }
}