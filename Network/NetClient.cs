﻿using Duality;
using LiteNetLib;
using System;
using LiteNetLib.Utils;

namespace Headmower.Network
{
    public class NetClient : Resource
    {
        public string ConnectKey { get; set; } = "MyNetGame";
        public string IPAddress { get; set; } = "127.0.0.1";
        public int Port { get; set; } = 1488;

        public EventBasedNetListener Listener { get; protected set; } = new EventBasedNetListener();
        public NetManager Client { get; protected set; }

        public EventHandler<NetworkReceiveEventArgs> NetworkReceiveEvent;

        protected override void OnLoaded()
        {
            base.OnLoaded();
            Client = new NetManager(Listener, ConnectKey);
            Client.SimulateLatency = true;
            Client.SimulationMaxLatency = 1500;
            Listener.NetworkReceiveEvent += OnNetworkReceiveEvent;
        }

        private void OnNetworkReceiveEvent(NetPeer peer, NetDataReader reader)
        {
            NetworkReceiveEvent?.Invoke(this, new NetworkReceiveEventArgs { Peer = peer, Reader = reader });
        }

        //public bool UnsyncedEvents { get => Client.UnsyncedEvents; set => Client.UnsyncedEvents = value; }
        public bool IsRunning { get => Client.IsRunning; }

        public void Start()
        {
            Client.Start();
        }

        public void Connect()
        {
            Client.Connect(IPAddress, Port);
        }

        public void PollEvents()
        {
            Client.PollEvents();
        }

        internal void Stop()
        {
            Client.Stop();
        }

        protected override void OnDisposing(bool manually)
        {
            Listener.NetworkReceiveEvent -= OnNetworkReceiveEvent;
            base.OnDisposing(manually);
        }

        internal void Send(NetDataWriter writer)
        {
            Client.GetFirstPeer().Send(writer, SendOptions.ReliableOrdered);
        }
    }
}