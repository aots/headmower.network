﻿using LiteNetLib;
using LiteNetLib.Utils;
using System;

namespace Headmower.Network
{
    public class NetworkReceiveEventArgs : EventArgs
    {
        public NetPeer Peer { get; set; }

        public NetDataReader Reader { get; set; }
    }
}