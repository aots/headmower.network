﻿using LiteNetLib.Utils;
using System;

namespace Headmower.Network
{
    public interface ICmpNetSyncable
    {
        void OnPacketReceived(NetDataReader reader);

        event EventHandler DataChangedEvent;

        void WritePacket(NetDataWriter writer);
    }
}