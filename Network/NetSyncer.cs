﻿using Duality;
using Duality.Resources;
using LiteNetLib.Utils;
using System;
using System.Collections.Generic;

namespace Headmower.Network
{
    public interface INetSyncer
    {
        //void SubscribePackets(NetSerializer serializer);

        void OnPacketReceived(NetDataReader reader);
    }

    public class NetSyncer : Component, ICmpInitializable, INetSyncer
    {
        private NetUpdater Updater { get; set; }
        private Dictionary<string, ICmpNetSyncable> SyncableList { get; set; } = new Dictionary<string, ICmpNetSyncable>();
        private NetDataWriter Writer { get; set; } = new NetDataWriter();

        public void OnInit(InitContext context)
        {
            if (context == InitContext.Activate)
            {
                Updater = Scene.Current.FindComponent<NetUpdater>();

                foreach (var comp in GameObj.GetComponents<ICmpNetSyncable>())
                {
                    SyncableList.Add(comp.GetType().Name, comp);
                    comp.DataChangedEvent += Comp_DataChangedEvent;
                }
                Updater.RegisterSyncer(this);
            }
        }

        private void Comp_DataChangedEvent(object sender, EventArgs e)
        {
            var comp = (ICmpNetSyncable)sender;
            var id = _findComp(comp);
            Writer.Put(id);
            comp.WritePacket(Writer);
            Updater.Send(this, Writer);
            Writer.Reset();
        }

        private string _findComp(ICmpNetSyncable comp)
        {
            foreach (var pair in SyncableList)
            {
                if (comp == pair.Value)
                    return pair.Key;
            }
            throw new KeyNotFoundException();
        }

        public void OnPacketReceived(NetDataReader reader)
        {
            var type = reader.GetString();
            SyncableList[type].OnPacketReceived(reader);
        }

        public void OnShutdown(ShutdownContext context)
        {
            if (context == ShutdownContext.Deactivate)
            {
                Updater.UnregisterSyncer(this);
            }
        }
    }
}