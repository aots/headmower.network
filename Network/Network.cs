﻿using Duality;

namespace Headmower.Network
{
    /// <summary>
    /// Defines a Duality core plugin.
    /// </summary>
    ///
    public class Network : CorePlugin
    {
        // Override methods here for global logic
    }
}